package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const (
	feedUrl   = "https://fep-api.dimensiondata.com/v2/stages/v2/280/activity-feed"
	imgUrlFmt = "https://racecenter.letour.fr/-/media/images/%v.ashx"
	newsflash = "Newsflash"
)

var (
	lastEventTime time.Time
)

func main() {

	for {
		c := http.DefaultClient
		c.Timeout = time.Second * 5

		resp, err := c.Get(feedUrl)
		if err != nil {
			log.Fatal(err)
		}

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		ar := ActivityResponse{}

		json.Unmarshal(body, &ar)

		for i := len(ar.Items) - 1; i > 0; i-- {
			if ar.Items[i].CreatedAt.After(lastEventTime) {
				PrintEvent(ar.Items[i])
			}
		}

		lastEventTime = ar.Items[0].CreatedAt

		<-time.After(time.Second * 5)
	}
}

func PrintEvent(e Event) {

	format := `%v 
%v
%v
%v

`

	if e.ContentType == newsflash {
		fmt.Printf(format, e.CreatedAt, e.Title, e.Description, fmt.Sprintf(imgUrlFmt, e.EventType))
	}

}

type ActivityResponse struct {
	Items []Event `json:"Items"`
}

type Event struct {
	CreatedAt   time.Time `json:"CreatedAt"`
	Title       string    `json:"Title"`
	ContentType string    `json:"ContentType"`
	EventType   string    `json:"EventType"`
	Description string    `json:"Description"`
}
